package com.boot.bookingrestaurantapi.services.impl;

import com.boot.bookingrestaurantapi.entities.Reservation;
import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.repositories.ReservationRespository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CancelReservationServiceImplTest {


    private static final String LOCATOR = "Burger 7";
    private static final String RESERVATION_DELETED = "LOCATOR_DELETED";
    private static final Reservation RESERVATION = new Reservation();

    @Mock
    ReservationRespository reservationRespository;

    @InjectMocks
    CancelReservationServiceImpl cancelReservationService;

    @Before
    public void init() throws BookingException {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void deleteReservationOKTest() throws BookingException{

        Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));
        Mockito.when(reservationRespository.deleteByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));

        final String response = cancelReservationService.deleteReservation(LOCATOR);
        assertEquals(response, RESERVATION_DELETED);
    }

    @Test(expected = BookingException.class)
    public void deleteReservationNotFoundErrorTest() throws BookingException{

        Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(Optional.empty());
        Mockito.when(reservationRespository.deleteByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));

        final String response = cancelReservationService.deleteReservation(LOCATOR);
        assertEquals(response, RESERVATION_DELETED);
        fail();
    }

    /*
    @Test(expected = BookingException.class)
    public void deleteReservationInternalServerErrorTest() throws BookingException{

        Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));

        //Mockito.when(reservationRespository.deleteByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));

        Mockito.doThrow(Exception.class).when(reservationRespository.deleteByLocator(LOCATOR));

        final String response = cancelReservationService.deleteReservation(LOCATOR);
        assertEquals(response, RESERVATION_DELETED);
        fail();
    }

     */
}