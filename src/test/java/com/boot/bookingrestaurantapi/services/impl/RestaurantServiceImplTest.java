package com.boot.bookingrestaurantapi.services.impl;


import com.boot.bookingrestaurantapi.entities.Board;
import com.boot.bookingrestaurantapi.entities.Reservation;
import com.boot.bookingrestaurantapi.entities.Restaurant;
import com.boot.bookingrestaurantapi.entities.Turn;
import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.jsons.RestaurantRest;
import com.boot.bookingrestaurantapi.jsons.TurnRest;
import com.boot.bookingrestaurantapi.repositories.RestaurantRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RestaurantServiceImplTest {

    //Objetos para la Entidad de Restaurant
    private  static final Long RESTAURANT_ID = 1L;
    public static final Restaurant RESTAURANT = new Restaurant();
    private static final String NAME = "Burger";
    private static final String DESCRIPTION = "Tipos de Hamburguesas";
    private static final String ADDRES = "AV Siempre Viva";
    private static final String IMAGES_URL = "localhos:8080/images";

    public static final List<Turn> TURN_LIST = new ArrayList<>();
    public static final List<Board> BOARD_LIST = new ArrayList<>();
    public static final List<Reservation> RESERVATION = new ArrayList<>();

    @Mock
    RestaurantRepository restaurantRepository;

    @InjectMocks
    RestaurantServiceImpl restaurantService;

    @Before
    public void init() throws BookingException{
        MockitoAnnotations.initMocks(this);
        RESTAURANT.setName(NAME);
        RESTAURANT.setDescription(DESCRIPTION);
        RESTAURANT.setAddress(ADDRES);
        RESTAURANT.setId(RESTAURANT_ID);
        RESTAURANT.setImage(IMAGES_URL);
        RESTAURANT.setTurns(TURN_LIST);
        RESTAURANT.setBoards(BOARD_LIST);
        RESTAURANT.setReservations(RESERVATION);
    }

    @Test
    public void getRestaurantByIdTest() throws BookingException{
        Mockito.when(restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.of(RESTAURANT));
        restaurantService.getRestaurantById(RESTAURANT_ID);
    }

    @Test(expected = BookingException.class)
    public void getRestaurantByIdFailedTest() throws BookingException{
        Mockito.when(restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.empty());
        restaurantService.getRestaurantById(RESTAURANT_ID);
        fail();
    }

    @Test
    public void getRestaurants() throws BookingException{
        final Restaurant restaurant = new Restaurant();
        Mockito.when(restaurantRepository.findAll()).thenReturn(Arrays.asList(restaurant));
        final List<RestaurantRest> response = restaurantService.getRestaurants();
        assertNotNull(response);
        assertFalse(response.isEmpty());
        assertEquals(response.size(), 1);
    }

}